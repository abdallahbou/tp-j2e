package com.example.exercise1j2e.services;

import com.example.exercise1j2e.models.Produit;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProduitService {



    private static List<Produit> produits = new ArrayList<>();

    private StandardServiceRegistry registre;

    private static SessionFactory sessionFactory;

    private static Session session;

    public ProduitService(){
        registre = new StandardServiceRegistryBuilder().configure().build();
        sessionFactory = new MetadataSources(registre).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();

    }

    public  boolean addProduit(Produit produit){
        return  produits.add(produit);
    }

    public  List<Produit> getProduits(){
        return produits;
    }

    public Produit getProduit(int id){return produits.get(id);}

    public static List<Produit> findAll() {
        session = sessionFactory.openSession();
        session.beginTransaction();
        Query<Produit> produitQuery = session.createQuery("from Produit");
        session.getTransaction().commit();
        return produitQuery.list();
    }

}
