package com.example.exercise1j2e;

import com.example.exercise1j2e.models.Produit;
import com.example.exercise1j2e.services.ProduitService;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "cc",value="/cc")
public class ProduitServlet extends HttpServlet {

    private List<Produit> produits;

    private ProduitService produitService;


    public void init(){
        produitService = new ProduitService();
        produits = produitService.findAll();
    }

    public  void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        response.setContentType("text/html");

        PrintWriter writer = response.getWriter();
        writer.println("<html><body>");

        for (Produit p : produits){
            writer.println("<div>");
            writer.println(p.getMarque());

            writer.println("</div>");
        }
//        writer.println("<h1>" + "message "+ "</h1>");

        writer.println("</body></html>");



    }

    public void destroy() {
    }

}
